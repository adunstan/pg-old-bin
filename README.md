Old Versions of Postgres
========================

These are binaries of old Postgres versions (from 7.2 up to 9.5,
at the time of writing the last version that is at End Of Life). I
arbitrarily picked 7.2 as the start point because it was the
first version of Postgres I ever used.

The have been tested and run on Fedora 31 and Ubuntu 20.04.

The binaries have been build on an instance of Fedora 7, and for all but 7.2
include contrib. They have been configured like this:

```
./configure --enable-debug --enable-cassert --prefix=/opt/pgsql/n.n
```

where `n.n` is the major release, e.g. `9.3`.

Releases from 8.0 on are relocatable, but before that they need to be in the
configured location. After unpacking them I put a soft link from `/opt/pgsql`
to the place where I unpacked them:

```
andrew@emma:bf $ ls -l /opt/pgsql
lrwxrwxrwx 1 root root 32 Apr 13 15:36 /opt/pgsql -> /home/andrew/pgl/pgold/opt/pgsql
```

Some binaries from 7.2 to 8.1 require `libnsl.so.1`. On my Fedora system I have
`libnsl.so.2`. However `snapd` has an older version, so I symlinked that into
the lib directory:

```
andrew@emma:pgsql $ ls -l 8.1/lib/libnsl.so.1 
lrwxrwxrwx 1 andrew andrew 63 Apr 13 15:47 8.1/lib/libnsl.so.1 -> /var/lib/snapd/snap/snapd/8542/lib/x86_64-linux-gnu/libnsl.so.1
```

These binaries have also been tested on Ubuntu 20.04. It has libnsl.so.1 so
the symlink isn't required.

The immediate use for these is to provide a test bed for version-aware
PostgresNode.pm, the fundamental building block for Postgres TAP tests.

Longer term it is intended to use them for testing how well `pg_dump` works
with old versions of Postgres, as it is supposed to do.

TODO
---

- add some data repos that we can run against.

